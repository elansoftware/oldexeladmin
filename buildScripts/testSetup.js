// This file isn't transpiled, so must use CommonJS and ES5

// Register babel. Tells mocha that babel should first transpile our tests before mocha runs those tests
require('babel-register')();

// Disable webpack-specific features that Mocha doesn't understand. In this case the 'require css' statement in index.js. "Just trreat it like an empty function."
require.extensions['css'] = function() {};


