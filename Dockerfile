FROM alpine
LABEL maintainer="itsupport@exelwines.co.uk"
RUN apk add --update nodejs nodejs-npm
COPY . /src
WORKDIR /src
RUN npm install
EXPOSE 4040
CMD [ "npm", "start" ]
